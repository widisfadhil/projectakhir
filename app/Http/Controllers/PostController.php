<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $this->validate($request,[
            'caption' => 'required|unique:posts',
            'image' => 'required|mimes:jpeg,jpg,png|max:2200'
        ]);

        $image = $request->image;
        $new_image=time().' - '.$image->getClientOriginalName();

        $query = DB::table('posts')->insert([
        "caption" => $request["caption"],
        "image" => $new_image,
        'user_id' => auth()->id() 
        ]);

        $image->move('upload/',$new_image);
        //$cast = new Cast;
        //$cast->nama = $request["nama"];
        //$cast->umur = $request["umur"];
        //$cast->bio = $request["bio"];
        //$cast->save();
        return redirect('/profile/' . auth()->user()->id);
    }

    public function edit(\App\Post $post)
    {
        return view('posts.edit',compact('post'));
    }

    public function update(\App\Post $post,Request $request)
    {
        $data = request()->validate([
            'caption' => 'required',
            'image' => '', 
        ]);
        $image = $request->image;
        $new_image=time().' - '.$image->getClientOriginalName();

        $update = Post::where('id',$post->id)->update([
            "caption" => $request["caption"],
            "image" => $new_image,
            'user_id' => auth()->id() 
        ]);

        $image->move('upload/',$new_image);
        return redirect("/profile/{$post->user_id}"); 
    }
    public function destroy(\App\Post $post){
        //$query = DB::table('cast')->where('id',$id)->delete();
         Post::destroy($post->id);
         return redirect("/profile/{$post->user_id}");
    }
}
