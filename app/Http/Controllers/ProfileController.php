<?php

namespace App\Http\Controllers;
use Intervention\Image\Facades\Image;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use App\Post;
use App\Comment;

class ProfileController extends Controller
{
    public function home(\App\User $user)
    {
        $id=auth()->id();
        return view('home',compact('user','id'));
    }

    public function index(\App\User $user )
    {
        $follows = (auth()->user()->following->contains($user->id));
        $datapost=Post::all();
        $datacomment=Comment::all();
        return view('profiles.index', compact('user', 'follows','datapost','datacomment'));
    }

    public function edit(\App\User $user)
    {
        return view('profiles.edit', compact('user'));
    }

    public function update(\App\User $user,Request $request)
    {
        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => '',  
        ]);
        $image = $request->image;
        $new_image=time().' - '.$image->getClientOriginalName();

        $update = Profile::where('id',$user->id)->update([
            "title" => $request["title"],
            "description" => $request["description"],
            "image" => $new_image,
            'user_id' => auth()->id() 
        ]);

        $image->move('upload/',$new_image);
        return redirect("/profile/{$user->id}"); 
    }
}
