<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Post;
use App\Comment;

class CommentController extends Controller
{
    public function create(){
        return view('comment.create');
    }
    public function store(Request $request){
        //dd($request->all());
        $this->validate($request,[
            'comment' => 'required|unique:comment',
        ]);

        $query = DB::table('comment')->insert([
        "comment" => $request["comment"],
        'user_id' => auth()->id(), 
        'posts_id'=> auth()->id()
        ]);
        return redirect('/profile/' . auth()->user()->id);
    }
}
