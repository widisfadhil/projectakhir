<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Post;

class Comment extends Model
{
    protected $table = "comment";
    protected $fillable = ['comment', 'user_id','posts_id'];

    public function users()
  {
   return $this->belongsToMany('App\User');
  }  
  public function post()
  {
   return $this->belongsToMany('App\Post');
  }
}
