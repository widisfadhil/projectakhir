@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
             <img src="{{asset('upload/'.$user->profile->image)}}" class="rounded-circle" width="200" height="200" alt="...">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                
                <div class="d-flex align-items-center pb-3">
                    <div class="h4">{{ $user->username }}</div>

                    <follow-button user-id="{{$user->id}}"></follow-button>
                </div>
                
                <a href="/p/create">Add New Post</a>
                
            </div>
            <a href="/profile/{{$user->id}}/edit">Edit Profile</a>
            <div class="d-flex">
                <div class="pr-5"><strong class="pr-1">{{$user->posts->count()}}</strong>posts</div>
                <div class="pr-5"><strong class="pr-1">{{ $user->profile->followers->count()}}</strong>followers</div>
                <div class="pr-5"><strong class="pr-1">{{ $user->following->count()}}</strong>following</div>
            </div>
            <div class="pt-4 font-weight-bold">{{ $user->profile->title }}</div>
            <div>{{ $user->profile->description }}</div>
        </div>
    </div>
    <div class="row pt-5">

        <div class="col-4 pb-4">
            <h2>Post</h2>
        @foreach ($datapost as $value)
            <strong>Image</strong><br>

            <img src="{{asset('upload/'.$value->image)}}" class="d-flex" width="400" height="400" alt="...">

            <hr>
            <strong>Caption</strong>

            <p>{{$value->caption}}</p>
            <hr>

            <strong>Comment</strong>
            @foreach ($datacomment as $comment)
            <p>{{$comment->comment}}</p>
            <hr>
            @endforeach

            <a class="btn btn-primary mt-1" href="/p/{{$value->id}}/edit">Edit Post</a>
            <a class="btn btn-primary mt-1" href="/comment/create">Add Comment</a>
            <form action="/p/{{$value->id}}" method="POST"> 
            @csrf
            @method('DELETE')
            <input type="submit" value="Delete" class="btn btn-danger btn sm mt-1">
            </form>
        </div>
        @endforeach
        
    </div>
</div>
@endsection
