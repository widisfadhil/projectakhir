@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br>
                <a class="btn btn-primary mt-3 mb-4 d-inline-flex p-2 bd-highlight" href="/profile/{{$id}}">Go to profile page</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
